(ns on-track.core
  (:require [reagent.core :as r]))


(defonce app-state
  (r/atom {:people
           {1 {:name "John Smith"}
            2 {:name "Maggie Johnson"}}}))

(defn people []
  (:people @app-state))

(defn person-keys []
  (println "RUN person-keys")
  (-> @(r/track people)
      keys
      sort))

(defn person [id]
  (-> @(r/track people)
      (get id)))

; (defn pure-person [db id]
;   (println "RUN person")
;   (-> (get db id)))


(defn name-comp [id]
  (println "CREATE name-comp")
  (let [p @(r/track person id)]
    [:li
     (:name p)]))

; (defn name-comp [id]
;   (println "CREATE name-comp")
;   (let [p @(do (println "TRACK person") (r/track person id))]
;     [:li
;      (:name p)]))

; (defn name-comp [id]
;   (println "CREATE name-comp")
;   (r/with-let [*p (do (println "TRACK person") (r/track person id))]
;     [:li
;      (:name @*p)]))

; (defn name-comp [db id]
;   (println "CREATE name-comp")
;   (r/with-let [*p (r/track pure-person db id)]
;     [:li
;      (:name @*p)]))

; (defn name-comp [db id]
;   (println "CREATE name-comp")
;   (let [p @(r/track pure-person db id)]
;     [:li
;      (:name p)]))

; (defn name-comp [db id]
;   (println "CREATE name-comp")
;   (let [p (pure-person db id)]
;     [:li
;      (:name p)]))

(defn name-list []
  (println "CREATE name-list")
  (let [ids @(r/track person-keys)]
    [:ul
     (for [i ids]
       ^{:key i} [name-comp i])]))

; (defn name-list []
;   (println "CREATE name-list")
;   (let [ids @(r/track person-keys)
;         db (:people @app-state)]
;     [:ul
;      (for [i ids]
;        ^{:key i} [name-comp db i])]))

;;;

(defn change-name-of-1-random []
  (swap! app-state
         assoc-in [:people 1 :name] (str (rand-int 1000000))))

(defn change-name-of-1-john-doe []
  (swap! app-state
         assoc-in [:people 1 :name] "John Doe"))

(defn doc-app []
  [:<>
   [:h1 "Example from Reagent doc"]
   [:hr]
   [name-list]
   [:hr]
   [:button
    {:on-click #(change-name-of-1-random)}
    "Change 1 name"]])

(defn render-doc-example []
  (r/render [doc-app] (js/document.getElementById "app")))

;;;
