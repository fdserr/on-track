# On Track

Demystifying Reagent `r/track`.

## Setup

Fire up a Figwheel REPL:
```
$ clj -Afw
```
Browse to `localhost:9500` if needed.
You should see Figwheel's Default Dev Page.

Spare some typing by sending expressions from `REPL.cljs` to the REPL.
YMMV regarding REPL results and printouts.

Spare even more typing by commenting/uncommenting definitions in `on-track.core` as you follow.

## Misconception #1

We can spare some CPU by wrapping a costly call in `r/track`. It will not run if the arguments did not change.

**NO.** Tracking (thanks God) does not prevent evaluation.

Fire a REPL and mount the example from [Reagent doc](http://reagent-project.github.io/docs/master/ManagingState.html) chapter: "The track function":
```
cljs.user=> (require '[on-track.core :as on-track])
nil
cljs.user=> (on-track/render-doc-example)
RUN person-keys
#object[on_track.core.doc_app]
```
We've added a trace to the tracked function `person-keys`:
```clojure
(defn person-keys []
  (println "RUN person-keys")
  (-> @(r/track people)
      keys
      sort))
```
Now, randomly change the name of person 1:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "699719"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "275586"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
```
As we can see, `person-keys` is ALWAYS evaluated. Let's try to NOT change values:
```
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
```
This time it does not run, but why? Let's instrument the component fn:
```clojure
(defn name-list []
  (println "RENDER name-list")
  (let [ids @(r/track person-keys)]
    [:ul
     (for [i ids]
       ^{:key i} [name-comp i])]))
```
And try again:
```
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
```
Reagent at work: NOTHING happens, not even a render, since we haven't changed anything. Let's change some stuff:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "741140"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
```
Our costly function still runs, but our main component does not render.

What we took for an optimisation is just normal Reagent job. No need for track.


## Misconception #2

It's okay to instantiate and deref a track in the same expression.

**NO.** Despite what's shown in the example, it's plain useless.

Let's instrument a track:
```clojure
(defn name-comp [id]
  (println "RENDER name-comp")
  (let [p @(do (println "CREATE track") (r/track person id))]
    [:li
     (:name p)]))
```

Re-mount the app:
```
cljs.user=> (on-track/render-doc-example)
CREATE name-list
CREATE name-comp
CREATE track
CREATE name-comp
CREATE track
#object[on_track.core.doc_app]
```

Let's see what happens when we change state:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "737026"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-comp
CREATE track person
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "687771"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-comp
CREATE track person
```
Obviously, we create a track object on each render.
We get its value, and since it changed, we re-render.

Of course, this won't happen if the value is equivalent:
```
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-comp
CREATE track person
cljs.user=> (on-track/change-name-of-1-john-doe)
{:people {1 {:name "John Doe"}, 2 {:name "Maggie Johnson"}}}
```
Because we don't render/create/call `name-comp` at all, not because of
some sort of track magic.  

Now let's move the track creation outside of render, using `r/with-let`, and deref it where we need the value:
```clojure
(defn name-comp [id]
  (println "CREATE name-comp")
  (r/with-let [*p (do (println "TRACK person") (r/track person id))]
    [:li
     (:name @*p)]))
```
IMPORTANT: we need to refresh the page to really re-mount this change.

Now we start again:
```
cljs.user=> (on-track/render-doc-example)
CREATE name-list
RUN person-keys
CREATE name-comp
TRACK person
CREATE name-comp
TRACK person
#object[on_track.core.doc_app]
```
We still create our two `track person` on mount, but what happens when we change state:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "736430"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-comp
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "652922"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-comp
```
No more track instantiation, no more garbage, no more collect.
If you think track is cheap, [reconsider](https://github.com/reagent-project/reagent/blob/f16ee70a24cc27cfa7bebc6d9286f27f8aa90f2a/src/reagent/ratom.cljs#L187).


## Misconception #3

Track can be used on pure functions.

**NO.** The doc says it: "[If the function derefs
Reagent atoms (or track, etc),...](http://reagent-project.github.io/docs/master/reagent.core.html#var-track)".

Let's purify `person`: it takes a db value and an id, and always evaluates to the same value, given the same args.
```clojure
(defn pure-person [db id]
  (println "RUN person")
  (-> (get db id)))
```
We'll pass `db` from the top component.
```clojure
(defn name-list []
  (println "CREATE name-list")
  (let [ids @(r/track person-keys)
        db (:people @app-state)]
    [:ul
     (for [i ids]
       ^{:key i} [name-comp db i])]))
```
Let's refresh the page and re-mount our app, then try some changes:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "471667"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-list
CREATE name-comp
CREATE name-comp
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "833522"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-list
CREATE name-comp
CREATE name-comp
```
Hooray, the costly `pure-person` function is not called anymore! But wait... look at the screen: THE CHANGES DON'T SHOW!

Let's move the tracking back to where it was before:
```clojure
(defn name-comp [db id]
  (println "CREATE name-comp")
  (let [p @(r/track pure-person db id)]
    [:li
     (:name p)]))
```
Refresh and re-mount (not shown), try some changes:
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "913642"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-list
CREATE name-comp
RUN person
CREATE name-comp
RUN person
```
Now we can see the changes. But wait... we're creating one too many `name-comp`, when only one name has changed! Compare to our previous correct version.

If there was only one instance of `name-comp`, no one would notice! In fact, there's no bonus whatsoever compared to using `pure-person` without track:
```clojure
(defn name-comp [db id]
  (println "CREATE name-comp")
  (let [p (pure-person db id)]
    [:li
     (:name p)]))
```
Let's try (refresh/re-mount not shown):
```
cljs.user=> (on-track/change-name-of-1-random)
{:people {1 {:name "920209"}, 2 {:name "Maggie Johnson"}}}
RUN person-keys
CREATE name-list
CREATE name-comp
RUN person
CREATE name-comp
RUN person
```

## Conclusion

`r/track` is a good way to optimise a chain of transformations when there's always a `r/atom` or such involved in each step, as the example shows. It's easy to get confused to think it'll spare function calls: it just doesn't work that way, use `clojure.core/delay` for that purpose. `track` should be seen the other way round: it generates function calls (reactions): it triggers React render on state change, but the state has to be computed first.

OTOH functional components are pure functions of their args, they don't need to watch any (global) state, nor a (reactive) derivation of such state. Choose wisely when to use `track` or not:

- No change in component args values = no React render; no need to `track`.
- `track` is not `delay`: `track` avoids useless React re-renders, computation always happens (unless none of the dependent `r/atoms` values has changed).
- `track` within a component body is a waste; move to `mount` using higher order component form, or better: `r/with-let`.
- If no `r/atom` (or such) is involved in `track`ed evaluation chain,  re-render is never triggered; therefore `track`ing a pure function call is misleading and can be dangerous.  
